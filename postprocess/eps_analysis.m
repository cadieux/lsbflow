% plot sgs dissipation tau_ij S_ij
clc;
clear all;
% read in grid for all results presented - assumes same mesh/resolution
gridfile = 'grid.dat';
[dim,xpts,ypts,zpts,gf] = readgridfile(gridfile);
nx = dim(1);
ny = dim(2);
nzp = dim(3);

% scale x coordinates
x_scaled = xpts/zpts(1);
% scale vertical coordinates
z_scaled = zpts/zpts(1);
% scale spanwise coordinates
y_scaled = ypts/zpts(1);

% 2D grid
for k=1:nzp
    xz(:,k) = x_scaled;
end
for i=1:nx
    zx(i,:) = z_scaled;
end
for j=1:ny
    xy(:,j) = x_scaled;
end
for i=1:nx
    yx(i,:) = y_scaled;
end


% eps_sgs{1} = importdata('eps_tnsa2.csv')';
% eps_sgs{2} = importdata('eps_udns_f1101.csv')';
% % eps_sgs{2} = -importdata('eps_sig.csv')';
% eps_sgs{3} = -importdata('eps_dsm.csv')';
% eps_sgs{4} = importdata('eps_udns_f161.csv')';

% eps_nu{1} = importdata('eps_nu_tns2.csv')';
% eps_nu{2} = importdata('eps_nu_filt_sharp.csv')';
% % eps_nu{2} = importdata('eps_nu_sig.csv')';
% eps_nu{3} = importdata('eps_nu_dsm.csv')';

eps_sgs{1} = importdata('eps_tns300_q5g_121.csv')';
eps_sgs{2} = importdata('eps_udns_q5g_1101.csv')';
% eps_sgs{2} = -importdata('eps_sig.csv')';
eps_sgs{3} = -importdata('eps_sgs_dsm2.csv')';
eps_sgs{4} = importdata('eps_udns_q5g_161_p2.csv')';

eps_nu{1} = importdata('eps_nu_tns300_q5g_121.csv')';
eps_nu{2} = importdata('eps_nu_udns_q5g_1101.csv')';
% eps_nu{2} = importdata('eps_nu_sig.csv')';
eps_nu{3} = importdata('eps_nu_dsm2.csv')';

for i=1:4
    log_eps_sgs{i} =  log(eps_sgs{i})/log(10);
end



figure(1)
max1 = max(max(log_eps_sgs{1}(30:nx,1:nzp)));
min1 = min(min(log_eps_sgs{1}(30:nx,1:nzp)));
intv = linspace(max1,max1-9*0.25,9);
contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_sgs{1}(30:nx,1:nzp),intv,'LineWidth',2.0);
axis([2.0 7.0 0 0.5]);
daspect([18 10 1]);
colormap('cool');
colorbar;
caxis([-7 -2]);
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
set(gcf, 'Position', [100, 100, 1000, 300]);
% title('time-averag contour plot u(x,z)');
saveas(1,'log_eps_tns300_q5g_121','epsc');

figure(2)
max1 = max(max(log_eps_sgs{2}(30:nx,1:nzp)));
min1 = min(min(log_eps_sgs{2}(30:nx,1:nzp)));
intv = linspace(max1-0.01,max1-0.01-8*0.25,8);
contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_sgs{2}(30:nx,1:nzp),intv,'LineWidth',2.0);
% contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_sgs{2}(30:nx,1:nzp),-3:-0.25:-5,'LineWidth',2.0);
axis([2.0 7.0 0 0.5]);
daspect([18 10 1]);
colormap('cool');
colorbar;
caxis([-7 -2]);
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
set(gcf, 'Position', [100, 100, 1000, 300]);
% title('time-averag contour plot u(x,z)');
% saveas(2,'log_eps_sig');
saveas(2,'log_eps_udns_q5g_1101','epsc');

figure(3)
max1 = max(max(log_eps_sgs{3}(30:nx,1:nzp)));
intv = linspace(max1,max1-9*0.25,9);
contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_sgs{3}(30:nx,1:nzp),intv,'LineWidth',2.0);
axis([2.0 7.0 0 0.5]);
daspect([18 10 1]);
colormap('cool');
colorbar;
caxis([-7 -2]);
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
set(gcf, 'Position', [100, 100, 1000, 300]);
% title('time-averag contour plot u(x,z)');
saveas(3,'log_eps_dsm2','epsc');

figure(4)
max1 = max(max(log_eps_sgs{4}(30:nx,1:nzp)));
intv = linspace(max1,max1-6*0.25,6);
contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_sgs{4}(30:nx,1:nzp),intv,'LineWidth',2.0);
axis([2.0 7.0 0 0.5]);
daspect([18 10 1]);
colormap('cool');
colorbar;
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
set(gcf, 'Position', [100, 100, 1000, 300]);
% title('time-averag contour plot u(x,z)');
saveas(4,'log_eps_udns_q5g_161','epsc');

% figure(2)
% v = [5E-9, 1E-8, 5E-8, 1E-7, 5E-7, 1E-6, 5E-6];
% contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),eps_sgs{1}(30:nx,1:nzp),v,'LineWidth',2.0);
% axis([2.0 7.0 0 0.5]);
% colorbar;
% xlabel('x','FontSize',8);
% ylabel('y','FontSize',8);
% % title('time-averag contour plot u(x,z)');
% saveas(2,'eps_tns');


log_eps_nu =  log(eps_nu{1})/log(10);


figure(5)
max1 = max(max(log_eps_nu(30:nx,1:nzp)));
intv = linspace(max1,max1-12*0.25,12);
contour(xz(30:nx,1:nzp),zx(30:nx,1:nzp),log_eps_nu(30:nx,1:nzp),intv,'LineWidth',2.0);
axis([2.0 7.0 0 0.5]);
daspect([18 10 1]);
colormap('cool');
colorbar;
caxis([-7 -2]);
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
set(gcf, 'Position', [100, 100, 1000, 300]);
% title('time-averag contour plot u(x,z)');
saveas(5,'log_eps_nu_tns300','epsc');