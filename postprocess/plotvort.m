% Create vorticity 2D and 3D plots
clc;
clear all;
% close all;

qfile = dir('q00021*');
gridfile = 'grid.dat';

% read in grid
[dim,xpts,ypts,zpts,g] = readgridfile(gridfile);

% read in velocity field
[t,dt,nx,ny,nz,q] = readsnapshot(qfile(1).name);

nq = 5;

% create 3D grid
x = zeros(nx,ny,nz);
y = zeros(nx,ny,nz);
z = zeros(nx,ny,nz);
% 
for i=1:nx
    for j=1:ny
        for k=1:nz
            x(i,j,k) = xpts(i)/zpts(1); 
            y(i,j,k) = ypts(j)/zpts(1);
            z(i,j,k) = zpts(k)/zpts(1);
        end
    end
end

% grid parameters
lx = ( xpts(nx) - xpts(1) )/zpts(1);
ly = ( ypts(ny) - ypts(1) )/zpts(1);
lz = 1.0;

% create 3D wavenumbers for FFT diff
kx = zeros(nx,ny,nz,nq);
ky = zeros(nx,ny,nz,nq);
kx(1:nx,1,1,1) = 2*pi/lx*[0:nx/2-1, nx/2, -nx/2+1:-1];
ky(1,1:ny,1,1) = 2*pi/ly*[0:ny/2-1, ny/2, -ny/2+1:-1];
for j=1:ny
    for k=1:nz
        for l=1:nq
            kx(:,j,k,l) = kx(:,1,1,1);
        end
    end
end
for i=1:nx
    for k=1:nz
        for l=1:nq
            ky(i,:,k,l) = ky(1,:,1,1);
        end
    end
end


% compute all derivatives in x
qf = fft(q,[],1);
dqidxjf = 1i*kx.*qf;
dqidxj(:,:,:,:,1) = real(ifft(dqidxjf,[],1));
% dx = x(2,1,1) - x(1,1,1);
% dqidxj(1,:,:,:,1) = 0.5*(q(2,:,:,:) - q(nx,:,:,:))/dx;
% for i=2:nx-1
%     dqidxj(i,:,:,:,1) = 0.5*(q(i+1,:,:,:) - q(i-1,:,:,:))/dx;
% end
% dqidxj(nx,:,:,:,1) = 0.5*(q(1,:,:,:) - q(nx-1,:,:,:))/dx;

% compute all derivatives in y
qf = fft(q,[],2);
dqidxjf = 1i*ky.*qf;
dqidxj(:,:,:,:,2) = real(ifft(dqidxjf,[],2));
% dy = y(1,2,1) - y(1,1,1);
% dqidxj(:,1,:,:,1) = 0.5*(q(:,2,:,:) - q(:,ny,:,:))/dy;
% for j=2:ny-1
%     dqidxj(:,j,:,:,1) = 0.5*(q(:,j+1,:,:) - q(:,j-1,:,:))/dy;
% end
% dqidxj(:,ny,:,:,1) = 0.5*(q(:,1,:,:) - q(:,ny-1,:,:))/dy;

% compute all derivative in z
for i=1:nx
    for j=1:ny
        for l=1:nq
%             qdz(1:nz) = q(i,j,1:nz,l);
%             qz(1) = 0.5*qz(1);
%             qz(nz) = 0.5*qz(nz);
%             dqdz = 2.*chebdif(qdz);
%             dqidxj(i,j,:,l,3) = g.*dqdz;
            dqidxj(i,j,:,l,3) = 2.*g.*chebdif(q(i,j,:,l));
            
        end
    end
end
dqidxj2(:,:,1,:,3) = (q(:,:,1,:) - q(:,:,1+1,:))/(zpts(1)-zpts(1+1));
for k=2:nz-1
    dqidxj2(:,:,k,:,3) = (q(:,:,k-1,:) - q(:,:,k+1,:))/(zpts(k-1)-zpts(k+1));
end
dqidxj2(:,:,nz,:,3) = (q(:,:,nz-1,:) - q(:,:,nz,:))/(zpts(nz-1)-zpts(nz));

% dqidxj2(:,:,1,:,3) = (q(:,:,1,:) - q(:,:,1+1,:))/(cos(pi*(1.-1.)/nz)-cos(pi*(2.-1.)/nz))*g(1);
% for k=2:nz-1
%     dqidxj2(:,:,k,:,3) = (q(:,:,k-1,:) - q(:,:,k+1,:))/(cos(pi*(k-1.)/nz)-cos(pi*(k+1.)/nz))*g(k);
% end
% dqidxj2(:,:,nz,:,3) = (q(:,:,nz-1,:) - q(:,:,nz,:))/(cos(pi*(nz-2.)/nz)-cos(pi*(nz-1.)/nz))*g(nz);


% figure(1)
% plot(xpts/zpts(1),q(:,1,nz-10,1),'-o',xpts/zpts(1),dqidxj(:,1,nz-10,1,1)/max(dqidxj(:,1,nz-10,1,1)),'-+');
% legend('u(x)','dudx(x)');
% 
% figure(2)
% plot(ypts/zpts(1),q(140,:,nz-10,1),'-sq',ypts/zpts(1),dqidxj(140,:,nz-10,1,2)/max(dqidxj(140,:,nz-10,1,2)),'-d');
% legend('u(y)','dudy(y)');
% 
% figure(3)
% qz(:,:) = q(:,1,:,1);
% dqz(:,:) = dqidxj(:,1,:,1,3);
% dqz2(:,:) = dqidxj2(:,1,:,1,3);
% plot(qz(60,:),zpts/zpts(1),'-sq',dqz(60,:),zpts/zpts(1),'-d',dqz2(60,:),zpts/zpts(1),'-d');
% legend('u(z)','cheb dudz(z)', 'fd dudz(z)');
% % 
% figure(4)
% xnu = 0.001;
% cf = xnu*dqidxj(:,1,nz,1,3)/(0.5*1.0);
% cf2 = xnu*dqidxj2(:,1,nz,1,3)/(0.5*1.0);
% plot(xpts/zpts(1), cf,xpts/zpts(1), cf2,'-o');

% Now compute some interesting things to plot

% Vorticity
vorti(:,:,:,1) = dqidxj(:,:,:,3,2) - dqidxj(:,:,:,2,3);
vorti(:,:,:,2) = dqidxj(:,:,:,1,3) - dqidxj(:,:,:,3,1);
vorti(:,:,:,3) = dqidxj(:,:,:,2,1) - dqidxj(:,:,:,1,2);

% Q-criterion
% q=-0.5*(dudx.^2+dvdy.^2+dwdz.^2)-dudy.*dvdx-dudz.*dwdx-dvdz.*dwdy;
% q_crit = zeros(nx,ny,nz);
% q_crit(:,:,:) = -0.5*(dqidxj(:,:,:,1,1).^2 + dqidxj(:,:,:,2,2).^2 + dqidxj(:,:,:,3,3).^2 ) ...
%                 - dqidxj(:,:,:,1,2).*dqidxj(:,:,:,2,1) - dqidxj(:,:,:,1,3).*dqidxj(:,:,:,3,1) ...
%                 - dqidxj(:,:,:,2,3).*dqidxj(:,:,:,3,2);
% 
% %Plotting a Q isosurface, Q=iso_q
% figure(5)
% iso_q=0.45; %Pick your number here
% p=patch(isosurface(x,y,z,q_crit,iso_q));
% set(p,'FaceColor','red','EdgeColor','none');
% daspect([1,1,1])
% axis([2 7.5 0 0.6 0 0.6]);
% % grid on
% % axis tight
% % ax = 1; ay = 1; az = 1;
% % view([ax,ay,az]);
% xlabel('x','FontSize',12);
% ylabel('z','FontSize',12);
% zlabel('y','FontSize',12);
% set(gca,'FontSize',12)
% % camroll(240)
% camlight 
% lighting gouraud
% saveas(5,'q_isosurface_1pc_tns','eps2c');
% 
% 
% %Plotting spanwise vorticity isosurface, Q=iso_q
figure(66)
iso_q=0.45; %Pick your number here
p=patch(isosurface(x(36:nx,:,5:nz),y(36:nx,:,5:nz),z(36:nx,:,5:nz),vorti(36:nx,:,5:nz,2),iso_q));
set(p,'FaceColor','green','EdgeColor','none');
daspect([1,1,1])
axis([2 7.5 0 0.6 0 0.6]);
% grid on
% axis tight
% ax = 1; ay = 1; az = 1;
% view([ax,ay,az]);
xlabel('x','FontSize',12);
ylabel('z','FontSize',12);
zlabel('y','FontSize',12);
set(gca,'FontSize',12)
% camroll(240)
camlight 
lighting gouraud
saveas(66,'vorty_isosurface_1pc_tns','epsc');


xz(1:nx,1:nz) = x(:,1,:);
xy(1:nx,1:ny) = x(:,:,1);
yx(1:nx,1:ny) = y(:,:,1);
zx(1:nx,1:nz) = z(:,1,:);

% plot contours of spanwise vorticity
figure(67)
contv = 0.0125:0.05:1;
vorty(1:nx,1:nz) = vorti(:,16,:,2);
contour(xz,zx,vorty./max(max(vorty)),contv,'LineWidth',0.5);
colorbar;
axis([0.5 7.5 0 0.5]);
daspect([3 1 1]);
xlabel('x','FontSize',8);
ylabel('y','FontSize',8);
set(gca,'FontSize',8)
% title('instantaneous contour plot vorticity in y-axis');
saveas(67,'contour_vorty_1pc_tns300','eps2c');

% plot contour of du/dy at wall
figure(68)
contour(xy,yx,dqidxj(:,:,nz,1,3)./max(max(dqidxj(:,:,nz,1,3))),contv,'LineWidth',1.0);
axis([0.5 7.5 0 0.6]);
daspect([2.5 1 1]);
colorbar;
xlabel('x','FontSize',8);
ylabel('z','FontSize',8);
set(gca,'FontSize',8)
% title('instantaneous contour plot of streamwise vertical velocity gradient du/dy');
saveas(68,'contour_dudy_wall_1pc_tns','eps2c');
        