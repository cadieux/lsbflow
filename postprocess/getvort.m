function [vortz]=getvort(q,x,y,nx,ny,g)
% Calculates vorticity (second order derivatives)
% Obtain velocities
u(1:nx,1:ny) = q(:,:,1);%./q(:,:,1);
v(1:nx,1:ny) = q(:,:,3);%./q(:,:,1);
% nym = ny+ny(1)-1;
nym = ny; %- 1;
% Compute du/dy
% dy1 = y(2) - y(1);
% dy2 = y(3) - y(2);
% dudy(:,1) = -(4*u(:,2) - u(:,3) -3*u(:,1))./(dy1+dy2);
% for j=2:ny-1
%     dy = y(j+1) - y(j-1);
%     dudy(:,j) = (u(:,j+1) - u(:,j-1))./dy;
% end
% dy1 = y(nym) - y(nym-1);
% dy2 = y(nym-1) - y(nym-2);
% dudy(:,nym) = (4*u(:,nym-1) - u(:,nym-2) -3*u(:,nym))./(dy1+dy2);

for i=1:nx
    dudy(i,:) = 2.*g.*chebdif(u(i,:));
end

% Compute dv/dx
dx1 = x(2) - x(1);
dx2 = x(3) - x(2);
dvdx(1,:) = (4*v(2,:) - v(3,:) -3*v(1,:))./(dx1+dx2);
for i=2:nx-1
    dx = x(i+1) - x(i-1);
    dvdx(i,:) = (v(i+1,:) - v(i-1,:))./dx;
end
dx1 = x(nx) - x(nx-1);
dx2 = x(nx-1) - x(nx-2);
dvdx(nx,:) = (4*v(nx-1,:) - v(nx-2,:) -3*v(nx,:))./(dx1+dx2);

vortz = dvdx - dudy;
    
end