% plot and compare results between postproc and otfavg
clc;
clear all;
% close all;

new_snapshots = 0;

if (new_snapshots==1)
    %% Read all data in
    
    % read in grid for all results presented - assumes same mesh/resolution
    gridfile = 'grid.dat';
    [dim,xpts,ypts,zpts,gf] = readgridfile(gridfile);
    nx = dim(1);
    ny = dim(2);
    nzp = dim(3);
    
    % set folder names where results of interest are - add more below if
    % necessary
    % qdir{2} = 'dsmag';
    % qdir{5} = 'inter_1-8-1';
    % qdir{5} = 'tnsa';
%     qdir{4} = 'sigma';
%     qdir{4} = 'udns_filt_161';
%     qdir{3} = 'dsm';
%     qdir{2} = 'tnsa';
    qdir{4} = 'udns_q5g_161';
    qdir{3} = 'dsm2';
    qdir{2} = 'udns_q5g_1101';
    qdir{1} = 'tns300_q5g121';
%     qdir{1} = 'udns';
%     qdir{1} = 'udns_filt_phys';
    % number of folders to explore
    ndir = max(size(qdir));
    
    % nqty is the number of qties stored in q: u,v,w,temp,p in our case = 5
    nqty = 5;
    
    % % set container for all data to zeroc
    qin = zeros(nx,ny,nzp,nqty,ndir);
    qp = zeros(nx,ny,nzp,nqty);
    qrms = zeros(nx,ny,nzp,3,ndir);
    
    % go into each folder and read in and average over each qavg file found
    for n=1:ndir
        filepath = strcat(qdir{n},'/qavg*'); % path to the files
        qavgfiles = dir(filepath); % get name of each individual file
        for l=1:max(size(qavgfiles))
            qavgfiles(l).name = strcat(qdir{n},'/',qavgfiles(l).name);
        end
        % read in each file and average them in time
        [t,dt,nx,ny,nzp,qin(:,:,:,:,n)] = readmanysnapshots(qavgfiles);
        
        % read in each instantaneous snapshot
        filepath = strcat(qdir{n},'/q0*'); % path to the files
        qfiles = dir(filepath); % get name of each individual file
        nt(n) = max(size(qfiles));
        for l=1:nt(n)
            qfiles(l).name = strcat(qdir{n},'/',qfiles(l).name);
            [t,dt,nx,ny,nzp,qp] = readsnapshot(qfiles(l).name);
            % u' = u - < u >
            qp = qp - qin(:,:,:,:,n);
            qrms(:,:,:,1:3,n) = qrms(:,:,:,1:3,n) + qp(:,:,:,1:3).^2/double(nt(n));
        end
    end
    
    % take spanwise average
    q(:,:,:,:) = sum(qin,2)./double(ny);
    
    % take square root of RMS
    qrms = sqrt(qrms);
    
    % some parameters
    nz = nzp-1;
    xnu = 0.001;
    u0 = 1.;
    den = 0.5*u0^2;
    
    
    % scale x coordinates
    x_scaled = xpts/zpts(1);
    % scale vertical coordinates
    z_scaled = zpts/zpts(1);
    % scale spanwise coordinates
    y_scaled = ypts/zpts(1);
%     save('complete_data.mat');
%     save('complete_data_filt_no_sigma.mat');
    save('jturb_tsfp9_data.mat');


else
    % load already analyzed data
%     load('time_avg_data.mat');
%     load('complete_data.mat');
%     load('complete_data_filt_no_sigma.mat');
    load('jturb_tsfp9_data.mat');

end

% load Spalart data for comparisons
load('SpalartDNS.mat');
    
%% Compute figures of merit: Cf, Cp, reattachment point, etc

% cpu speed from files
cpu_rel_cost(1:ndir) = 1.;
cpu_rel_cost(2) = 1.2272909164;%1.1525742202;
cpu_rel_cost(3) = 2.5381718873;
cpu_rel_cost(4) = 1.4055912007;
% cpu_rel_cost(5) = 2.128300139;

% compute du/dz
for i=1:nx
    for n=1:ndir
%         tmp(1:nzp) = q(i,nzp:-1:1,1,n);
        tmp(1:nzp) = q(i,:,1,n);
%         tmp(1) = 0.5*tmp(1);
%         tmp(nzp) = 0.5*tmp(nzp);
%         dtmpdz(nzp:-1:1) = -2.*chebdif(tmp);
        dtmpdz = 2.*chebdif(tmp);
%         dtmpdz(1) = 2*dtmpdz(1);
%         dtmpdz(nzp) = 2*dtmpdz(nzp);
        dudz_cheb(i,:,n) = dtmpdz.*gf;
%         dudz(i,:,n) = gf.*chebdif(q(i,:,1,n));
    end
end
clear tmp;

for i=1:nx
    for n=1:ndir
        dudz(i,1,n) = (q(i,1,1,n) - q(i,1+1,1,n))/(zpts(1)-zpts(1+1));
        for k=2:nzp-1
            dudz(i,k,n) = (q(i,k-1,1,n) - q(i,k+1,1,n))/(zpts(k-1)-zpts(k+1));
        end
        dudz(i,nzp,n) = (q(i,nzp-1,1,n) - q(i,nzp,1,n))/(zpts(nzp-1)-zpts(nzp));
    end
end

% use chebyshev
dudz = dudz_cheb;

% calc Cf
dz = zpts(nz)-zpts(nzp);
dx = xpts(2) - xpts(1);
dy = ypts(2) - ypts(1);
% tau_w(1:nx,1:ndir) = xnu*dudz(:,nzp,:);
tau_w(1:nx,1:ndir) = xnu*dudz_cheb(:,nzp,:);
% tau_w(1:nx,1:ndir) = xnu*(q(:,nz,1,:)-q(:,nzp,1,:))/dz;

cf = tau_w/den;

dzplus_x = zpts(nz)*sqrt(0.5*abs(cf))/xnu;

% calc u+,y+, log law of the wall
u_tau = sqrt(abs(tau_w));
dz_ind = find(x_scaled<=7.,1,'last');
i1 = find(x_scaled<=4.,1,'last');
i2 = find(x_scaled<=7.5,1,'last');
for l=1:ndir
    for i=1:nx
        zplus(i,1:nzp,l) = u_tau(i,l)*zpts(1:nzp)/xnu;
        uplus(i,1:nzp,l) = q(i,1:nzp,1,l)/u_tau(i,l);
        loglaw(i,1:nzp,l) = 1./0.387*log(zpts(1:nzp)*u_tau(i,l)/xnu)+4.4;
    end
    dzplus(l) = max(u_tau(i1:i2,l))*dz/xnu;
    dxplus(l) = max(u_tau(i1:i2,l))*dx/xnu;
    dyplus(l) = max(u_tau(i1:i2,l))*dy/xnu;
end

% dz_ind = find(x_sp<=7.,1,'last');
% dzplus_sp = zplus(dz_ind,nz,l);
i1 = find(x_sp<=4.,1,'last');
i2 = find(x_sp<=7.5,1,'last');
dxplus_sp = sqrt(0.5*max(cf_sp(i1:i2)))*abs(x_sp(2)-x_sp(1))*zpts(1)/xnu;
dyplus_sp = sqrt(0.5*max(cf_sp(i1:i2)))*0.6*zpts(1)/120./xnu;


% calc Cp
p_ind = nzp;


for l=1:ndir
%     cp(1:nx,l) = (q(:,p_ind,5,l) - min(q(1:nx/10,1,5,l)))/den;
%     cp(1:nx,l) = (q(:,p_ind,5,l) - q(nx/10,1,5,l))/den;
    cp(1:nx,l) = (q(:,p_ind,5,l) - min(q(1:nx/4,p_ind,5,l)))/den;
%     cp(1:nx,l) = (q(:,p_ind,5,l) - min(q(1:nx/4,p_ind,5,l)) +0.0025)/den;
%     cp(1:nx,l) = (q(:,p_ind,5,l) - q(nx/10,1,5,l))/den;
%     cp(1:nx/10,l) = (q(1:nx/10,p_ind,5,l) - q(1:nx/10,1,5,l))/den;
end
cp(1:nx,3) = (q(:,p_ind,5,3) - min(q(1:nx/4,p_ind,5,3))+0.0025)/den
% for l=2:3
% cp(1:nx,l) = (q(:,p_ind,5,l) - min(q(1:nx/4,p_ind,5,l),[],1) +0.0025)/den;
% end
% compute separation point
for l=1:ndir
    if (min(cf(1:nx-nx*0.2,l))<0.0)
        s_index(l) = find(cf(1:nx-nx*0.2,l)<0.0,1,'first');
        s_point(l) = x_scaled(s_index(l));
    else
        s_index(l) = 1;
        s_point(l) =  x_scaled(s_index(l));
    end
end
% compute Spalart reattachment point
s_index_sp = find(cf_sp(1:int64(1022/1.8))<0.0, 1, 'first');
s_point_sp = x_sp(s_index_sp);

% compute separation point % error
for l=1:ndir    
    separation_percent_error(l) = abs( s_point_sp - s_point(l) ) / s_point_sp * 100;
end


% compute peak negative skin friction
for l=1:ndir
    peak_cf(l) = min( cf(1:int64(nx/1.8),l) );
end
peak_cf_sp = min( cf_sp(1:int64(1022/1.8)) );

% compute peak neg skin friction % error 
for l=1:ndir    
    peak_percent_error(l) = -abs( peak_cf_sp - peak_cf(l) ) / peak_cf_sp * 100;
end

% compute reattachment point
for l=1:ndir
    if (min(cf(1:nx-nx*0.2,l))<0.0)
        r_index(l) = find(cf(1:nx-nx*0.2,l)<0.0,1,'last');
        r_point(l) = x_scaled(r_index(l));
    else
        r_index(l) = 1;
        r_point(l) =  x_scaled(r_index(l));
    end
end
% compute Spalart reattachment point
r_index_sp = find(cf_sp(1:int64(1022/1.8))<0.0, 1, 'last');
r_point_sp = x_sp(r_index_sp);

% compute reattachment point % error
for l=1:ndir    
    reattach_percent_error(l) = abs( r_point_sp - r_point(l) ) / r_point_sp * 100;
end

% compute L2 norm of Cp and Cf between index i1 and i2
i1 = find(x_scaled>=2.0,1,'first');
i2 = find(x_scaled>=7.5,1,'first');
sample_size = double( i2- i1 );
for l=1:ndir
    for i=1:nx
        [x_sp_loc,i_sp] = min(abs(x_sp - x_scaled(i)));
        cp_rel_err(i,l) = ( cp(i,l) - cp_sp(i_sp) )/ cp_sp(i_sp) * 100.;
        cf_rel_err(i,l) = ( cf(i,l) - cf_sp(i_sp) )/ ( cf_sp(i_sp) ) * 100.;

%         if (cf_sp(i_sp)>1E-5) 
%             cf_rel_err(i,l) = ( cf(i,l) - cf_sp(i_sp) )/ ( cf_sp(i_sp) + min(cf_sp) ) * 100.;
%         else
%             cf_err(i,l) = ( cf(i,l) - cf_sp(i_sp) );
%         end

        cp_l2norm(i,l) = ( cp(i,l) - cp_sp(i_sp) )^2.;
        cf_l2norm(i,l) = ( cf(i,l) - cf_sp(i_sp) )^2.;
    end
    
    
    cp_avg_rel_err(l) = sum(abs(cp_rel_err(i1:i2,l)))/sample_size;
    cp_rms(l) = sum(abs(cp_l2norm(i1:i2,l)))/sample_size;
    cf_avg_rel_err(l) = sum(abs(cf_rel_err(i1:i2,l)))/sample_size;
%     cf_avg_err(l) = sum(abs(cf_err(i1:i2,l)))/sample_size;
    cf_rms(l) = sum(cf_l2norm(i1:i2,l))/sample_size;
end


i1 = find(x_scaled>=5.5,1,'first');
i2 = find(x_scaled>=7.5,1,'first');
sample_size = double( i2- i1 );
for l=1:ndir
    cf_turb_avg_rel_err(l) = sum(abs(cf_rel_err(i1:i2,l)))/sample_size;
end


% obtain spanwise vorticity
for l=1:ndir    
    vorty(1:nx,1:nzp,l) = getvort(q(:,:,:,l),xpts,zpts,nx,nzp,gf);
end

% % dstar
% calc d* using velocity directly
dblas = 1.72*sqrt(xpts*xnu/u0);
dblas = dblas/zpts(1);


% calculate dstar using pseudo velocity u_p

% compute pseudo velocity u_p(y) = integral (-vort_y(y') )dy' from y'=0,y
u_p = zeros(nx,nzp,ndir);
for l=1:ndir
    for i=1:nx
        integral = 0.;
        for k=nz:-1:1
            dz = zpts(k) - zpts(k+1);
            integral = integral - (vorty(i,k,l) + vorty(i,k+1,l))*0.5*dz;
            u_p(i,k,l) = integral;
        end
        u_p(i,1,l) = u_p(i,2,l);
    end
end
% compute dstar_p
dstar_p(1:nx,1:ndir)=0;
theta_p(1:nx,1:ndir)=0;
ktop(1:nx) = 5;
ktop(1:nx/2.9) = 6;
ktop(1:nx/3.1) = 7;
for l=1:ndir
    for i=1:nx
        dsum=0.;
        sum2=0.;
        u_infty = u_p(i,ktop(i),l);
        for k=1:nz
            dsum=dsum-0.5*( 1 - u_p(i,k,l)/u_infty + 1 - u_p(i,k+1,l)/u_infty )*(zpts(k+1)-zpts(k));
            sum2=sum2-0.5*( u_p(i,k,l)/u_infty - (u_p(i,k,l)/u_infty)^2 + u_p(i,k+1,l)/u_infty - (u_p(i,k+1,l)/u_infty)^2 )*(zpts(k+1)-zpts(k));
        end
        dstar_p(i,l)=dsum;
        theta_p(i,l)=sum2;
    end
end

% scale by ceiling height
dstar = abs(dstar_p)/zpts(1);
theta = abs(theta_p)/zpts(1);

shape_factor = dstar./theta;

for l=1:ndir
    shape_factor_reattachment(l) = shape_factor(r_index(l),l);
end

% compute shape factor at reattachment % error
for l=1:ndir    
    shape_factor_percent_error(l) = abs( 3.1 - shape_factor_reattachment(l) ) / 3.1 * 100;
end

% compute overall gain in accuracy per cpu cost
for l=2:ndir
%     accuracy_gain(l) = 0.5*( cf_avg_rel_err(1) - cf_avg_rel_err(l) ) + 0.5*( cp_avg_rel_err(1) - cp_avg_rel_err(l) );
%     accuracy_gain(l) = ( cf_rms(1) - cf_rms(l) ) + ( cp_rms(1) - cp_rms(l) );
    accuracy_gain(l) = ( (peak_percent_error(1) - peak_percent_error(l)) + ...
        (reattach_percent_error(1) - reattach_percent_error(l)) + ...
        (separation_percent_error(1) - separation_percent_error(l)) + ...
        (shape_factor_percent_error(1)-shape_factor_percent_error(l)) + ...
        (cp_avg_rel_err(1)-cp_avg_rel_err(l)) + ...
        (cf_turb_avg_rel_err(1)-cf_turb_avg_rel_err(l)) )/6.;
    accuracy_gain_per_cpu_cost(l) =  accuracy_gain(l)/cpu_rel_cost(l);
end


dstar_p = dstar_p/zpts(1);
theta_p = theta_p/zpts(1);

% figure(111)
% plot(x_scaled,dstar_p,x_sp,dstar_sp,'-k','LineWidth',2.0);
% legend(qdir{:},'Spalart');

% figure(112)
% plot(u_p(1:10:nx,:,2),z_scaled);
% title('pseudo velocity vs z');


%% plot results

cf0(1:nx) = 0.;
color{4} = [0 1 1];
color{3} = [0.75 0 0];
color{2} = [0 0 1];
color{1} = [0 0.5 0];
symbols{4} = '--';
symbols{3} = 'r-.';
symbols{2} = 'b-';
% symbols{2} = '-';
% symbols{1} = 'k-';
symbols{1} = '-';

% Cf
figure(1)
plot(x_sp(1:15:1022),cf_sp(1:15:1022),'ok',x_sp(451:5:600),cf_sp(451:5:600),'ok','MarkerFaceColor','k','LineWidth',1.0);
hold on
plot(x_scaled,cf0,'k','LineWidth',2.0);
% plot(x_scaled,cf(:,1),'.',x_scaled,cf(:,2),'-',x_scaled,cf(:,3),'-.',x_scaled,cf(:,4),'--',x_scaled,cf(:,5),'g-','LineWidth',2.0);
% plot(x_scaled(1:10:nx),cf(1:10:nx,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,cf(:,1),'-',x_scaled,cf(:,2),'-',x_scaled,cf(:,3),'-.',x_scaled,cf(:,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,cf(:,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),cf(1:10:nx,2),'bd','MarkerFaceColor','b','LineWidth',1.0);
hold off
axis([0.5 7.5 -0.0035 0.0045]);
set(gca,'FontSize',14)
grid on
xlabel('x','FontSize',14);
ylabel('C_f','FontSize',14);
% legend(qdir{:},'Spalart','Location','SouthWest');
% saveas(1,'cf_comparison');
% saveas(1,'cf_filt_1pc','epsc');
saveas(1,'cf_filt_v2','epsc');

% Cp
figure(2)
plot(x_sp(1:15:1022),cp_sp(1:15:1022),'ok',x_sp(451:5:600),cp_sp(451:5:600),'ok','MarkerFaceColor','k','LineWidth',1.0);
hold on
% plot(x_scaled,cp(:,1),'.',x_scaled,cp(:,2),'-',x_scaled,cp(:,3),'-.',x_scaled,cp(:,4),'--',x_scaled,cp(:,5),'g-','LineWidth',2.0);
% plot(x_scaled(1:10:nx),cp(1:10:nx,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,cp(:,1),'-',x_scaled,cp(:,2),'-',x_scaled,cp(:,3),'-.',x_scaled,cp(:,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,cp(:,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),cp(1:10:nx,2),'bd','MarkerFaceColor','b','LineWidth',2.0);
hold off
% plot(x_scaled,cp(:,1:4),x_sp(1:20:1022),cp_sp(1:20:1022),'ok',x_sp(451:10:650),cp_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',2.0)
axis([0.5 7.5 -0.005 0.5]);
set(gca,'FontSize',14)
grid on
xlabel('x','FontSize',14);
ylabel('C_p','FontSize',14);
% legend(qdir{:},'Spalart','Location','NorthWest')
% saveas(2,'cp_comparison');
% saveas(2,'cp_filt_1pc','epsc');
saveas(2,'cp_filt_v2','epsc');


% contour plot of streamwise velocity
for k=1:nzp
    xz(:,k) = x_scaled;
end
for i=1:nx
    zx(i,:) = z_scaled;
end
for j=1:ny
    xy(:,j) = x_scaled;
end
for i=1:nx
    yx(i,:) = y_scaled;
end
% figure(3)
% contour(xz,zx,q(:,:,1,2),20,'LineWidth',2.0);
% axis([0.5 7.5 0 0.3]);
% colorbar;
% xlabel('x','FontSize',14);
% ylabel('y','FontSize',14);
% % title('time-averag contour plot u(x,z)');
% saveas(3,'countour_u_avg');
% 
% figure(333)
% contour(xz,zx,vorty(:,:,2)/max(max(abs(vorty(:,:,2)))),50,'LineWidth',2.0);
% axis([0.5 7.5 0 0.3]);
% colorbar;
% xlabel('x','FontSize',14);
% ylabel('y','FontSize',14);
% % title('time-averag contour plot u(x,z)');
% saveas(333,'countour_vorty_avg');

% plot some mean streamwise velocity profiles to augment contour plot with
% zero_u(1:nzp) = 0.0;
% figure(30)
% plot(zero_u,z_scaled,'k-', zero_u+1.2,z_scaled,'k-', zero_u+2*1.2,z_scaled,'k-', zero_u +3*1.2,z_scaled, 'k-','LineWidth',2.0);
% plot(q(1,:,1,2),z_scaled,'k-', q(67,:,1,2)+1.2,z_scaled,'k-', q(91,:,1,2)+2*1.2,z_scaled,'k-', q(139,:,1,2)+3*1.2,z_scaled, 'k-','LineWidth',2.0);
% axis([-0.2 4*1.2 0 0.3]);
% grid on
% xlabel('<u(y)>','FontSize',14);
% ylabel('y','FontSize',14);
% % legend('x=0.4','x=3','x=4','x=6');
% saveas(30,'lsb_vel_profile');

% figure(31)
% subplot(1,4,1);
% plot(q(45,:,1,2),z_scaled,'LineWidth',2.0);
% axis([-0.3 1.05 0 0.3]);
% xlabel('<u>(x=2)','FontSize',14);
% ylabel('y','FontSize',14);
% grid on;
% % saveas(31,'laminar_vel_profile');
% 
% % plot some mean streamwise velocity profiles to augment contour plot with
% % % figure(32)
% subplot(1,4,2);
% plot(q(67,:,1,2),z_scaled,'LineWidth',2.0);
% axis([-0.3 1.05 0 0.3]);
% xlabel('<u>(x=3)','FontSize',14);
% ylabel('y','FontSize',14);
% grid on;
% % saveas(32,'lsb_vel_profile');
% 
% % plot some mean streamwise velocity profiles to augment contour plot with
% % % figure(33)
% subplot(1,4,3);
% plot(q(91,:,1,2),z_scaled,'LineWidth',2.0);
% axis([-0.3 1.05 0 0.3]);
% xlabel('<u>(x=4)','FontSize',14);
% ylabel('y','FontSize',14);
% grid on;
% % saveas(33,'inflectional_vel_profile');
% 
% % % figure(34)
% subplot(1,4,4);
% plot(q(139,:,1,2),z_scaled,'LineWidth',2.0);
% axis([-0.3 1.05 0 0.3]);
% xlabel('<u>(x=6)','FontSize',14);
% ylabel('y','FontSize',14);
% grid on;
% % saveas(34,'turbulent_vel_profile');
% 
% saveas(31,'vel_profiles');

% plot d*
figure(4)
plot(x_sp(1:20:1022),dstar_sp(1:20:1022),'ok',x_sp(451:10:650),dstar_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',1.0);
hold on
plot(x_scaled(1:5:nx),dblas(1:5:nx),'-sqk','MarkerFaceColor','k','LineWidth',1.0);
% plot(x_scaled(1:10:nx),dstar_p(1:10:nx,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,dstar_p(:,1),'-',x_scaled,dstar_p(:,2),'-',x_scaled,dstar_p(:,3),'-.',x_scaled,dstar_p(:,4),'--','LineWidth',2.0);
% plot(x_scaled,dstar(:,1),'.',x_scaled,dstar(:,2),'-',x_scaled,dstar(:,3),'-.',x_scaled,dstar(:,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,dstar(:,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),dstar(1:10:nx,2),'bd','MarkerFaceColor','b','LineWidth',1.0);
hold off
% plot(x_scaled(1:5:nx),dblas(1:5:nx),'-sqk',x_scaled,dstar(:,1:4),x_sp(1:20:1022),dstar_sp(1:20:1022),'ok',x_sp(451:10:650),dstar_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',2.0)
axis([0.5 7.5 0.0 0.2]);
set(gca,'FontSize',14)
grid on
xlabel('x','FontSize',14);
ylabel('\delta^*','FontSize',14);
% legend('Blasius',qdir{:},'Spalart','Location','NorthWest')
% saveas(4,'dstar');
% saveas(4,'dstarp_filt_1pc','epsc');
saveas(4,'dstarp_filt_v2','epsc');

figure(44)
plot(x_sp(1:20:1022),theta_sp(1:20:1022),'ok',x_sp(451:10:650),theta_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',1.0)
hold on
% plot(x_scaled(1:10:nx),theta_p(1:10:nx,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,theta_p(:,1),'-',x_scaled,theta_p(:,2),'-',x_scaled,theta_p(:,3),'-.',x_scaled,theta_p(:,4),'--','LineWidth',2.0);
% plot(x_scaled,theta(:,1),'.',x_scaled,theta(:,2),'-',x_scaled,theta(:,3),'-.',x_scaled,theta(:,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,theta(:,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),theta(1:10:nx,2),'bd','MarkerFaceColor','b','LineWidth',1.0);
hold off
% plot(x_scaled,theta(:,1:4),x_sp(1:20:1022),theta_sp(1:20:1022),'ok',x_sp(451:10:650),theta_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',2.0)
% axis([0.5 7.5 -0.01 0.07]);
axis([0.5 7.5 0.0 0.07]);
set(gca,'FontSize',14)
grid on
xlabel('x','FontSize',14);
ylabel('\theta','FontSize',14);
% legend(qdir{:},'Spalart','Location','NorthWest')
% saveas(44,'theta');
% saveas(44,'thetap_filt_1pc','epsc');
saveas(44,'thetap_filt_v2','epsc');

% figure(45)
% plot(x_scaled,dblas,x_scaled,dstar_p,x_sp(1:20:1022),dstar_sp(1:20:1022),'ok',x_sp(451:10:650),dstar_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',2.0)
% axis([0.5 7.5 0.0 0.3]);
% % plot(x_sp,dstar_sp,x_scaled,dstar,x_scaled,dstarb,x_scaled,dstaro,xi,dblas);
% title('Displacement thickness vs x based on pseudo-velocity');
% xlabel('x');
% ylabel('\delta^*');
% legend('Blasius',qdir{:},'Spalart','Location','NorthEast')
% % legend('Spalart','UDNS','IC','UDNS OTF','Blasius');
% saveas(45,'dstar_p');
% 
% figure(46)
% plot(x_scaled,theta_p,x_sp(1:20:1022),theta_sp(1:20:1022),'ok',x_sp(451:10:650),theta_sp(451:10:650),'ok','MarkerFaceColor','k','LineWidth',2.0)
% % axis([0.5 7.5 0.0 0.1]);
% % plot(x_sp,dstar_sp,x_scaled,dstar,x_scaled,dstarb,x_scaled,dstaro,xi,dblas);
% title('Momentum thickness vs x based on pseudo-velocity');
% xlabel('x');
% ylabel('\theta');
% legend(qdir{:},'Spalart','Location','NorthWest')
% % legend('Spalart','UDNS','IC','UDNS OTF','Blasius');
% saveas(46,'theta_p');



% plot log law of the wall for comparison
zp(1:nzp,1:ndir) = zplus(dz_ind,:,:);
up(1:nzp,1:ndir) = uplus(dz_ind,:,:);
figure(70)
% semilogx(zplus(dz_ind,:,ndir),zplus(dz_ind,:,ndir),'-*k',zplus(dz_ind,1:nzp-7,ndir),loglaw(dz_ind,1:nzp-7,ndir),'-k','LineWidth',2.0)
semilogx(zplus(dz_ind,1:nzp,ndir),loglaw(dz_ind,1:nzp,ndir),'--k','LineWidth',2.0)
hold on
% plot(zplus(dz_ind,1:5:nzp-7,ndir),loglaw(dz_ind,1:5:nzp-7,ndir),'sqk','LineWidth',2.0)

% semilogx(zp(1:5:nzp,1),up(1:5:nzp,1),'bd','MarkerFaceColor','b','LineWidth',2.0)
% semilogx(zp(:,1),up(:,1),'-',zp(:,2),up(:,2),'-',zp(:,3),up(:,3),'-.',zp(:,4),up(:,4),'--','LineWidth',2.0)
for n=1:ndir
    semilogx(zp(:,n),up(:,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
semilogx(zp(1:5:nzp,2),up(1:5:nzp,2),'bd','MarkerFaceColor','b','LineWidth',1.0)
hold off
% semilogx(zp(:,1:ndir),up(:,1:ndir),zplus(dz_ind,:,2),zplus(dz_ind,:,2),'-*k',zplus(dz_ind,:,2),loglaw(dz_ind,:,2),'-sqk','LineWidth',2.0)
axis([0 max(max(zplus(dz_ind,:,:))) 0 25]);
set(gca,'FontSize',10)
daspect([20 1 1]);
grid on
xlabel('y+','FontSize',10);
ylabel('u+','FontSize',10);
% legend('u+=y+','Log Law',qdir{:},'Location', 'NorthWest');
% saveas(70,'loglaw');
% saveas(70,'loglaw_filt_1pc','epsc');
saveas(70,'loglaw_filt_v2','epsc');

% plot log law of the wall for different x locations for UDNS to prove it
% goes to equilibrium due to longer relaxation region
% xloc(4) = find(x_scaled>=7.0,1,'first');
% xloc(3) = find(x_scaled>=6.0,1,'first');
% xloc(2) = find(x_scaled>=5.0,1,'first');
% xloc(1) = find(x_scaled>=4.0,1,'first');
% nxloc = length(xloc);
% 
% symbols{4} = 'b.';
% symbols{3} = 'r-.';
% symbols{2} = 'g--';
% symbols{1} = 'b-';
% 
% for i=1:nxloc
%     zp_udns(1:nzp,i) = zplus(xloc(i),:,1);
%     up_udns(1:nzp,i) = uplus(xloc(i),:,1);
%     str = num2str(x_scaled(xloc(i)));
%     name{i} = strcat('x = ',str);
% end
% figure(71)
% % for i=1:nxloc
% %     figure(70+i)
%     semilogx(zp_udns(:,4),zp_udns(:,4),'-*k',zp_udns(1:nzp-7,4),loglaw(xloc(4),1:nzp-7,1),'-sqk','LineWidth',2.0)
%     hold on
% for i=1:nxloc
%     semilogx(zp_udns(:,i),up_udns(:,i),symbols{i},'LineWidth',2.0)
% end
%     semilogx(zp(:,4),up(:,4),'LineWidth',2.0);
%     axis([0 max(max(zplus(dz_ind,:,:))) 0 25]);
%     grid on
%     xlabel('y+','FontSize',14);
%     ylabel('u+','FontSize',14);
%     legend('u+=y+','Log Law',name{:},'\sigma -model x = 7.0282','Location', 'NorthWest');
%     hold off
%     title('UDNS u^{+} at different streamwise locations downstream of reattachment');
% end

% semilogx(zp(:,1:ndir),up(:,1:ndir),zplus(dz_ind,:,2),zplus(dz_ind,:,2),'-*k',zplus(dz_ind,:,2),loglaw(dz_ind,:,2),'-sqk','LineWidth',2.0)



% figure(70)
% for l=1:2
%     subplot(2,1,l);
%     semilogx(zplus(dz_ind,:,l),zplus(dz_ind,:,l),zplus(dz_ind,:,l),loglaw(dz_ind,:,l),zplus(dz_ind,:,l),uplus(dz_ind,:,l))
%     axis([0 max(max(zplus(dz_ind,:,:))) 0 25]);
%     grid on
%     xlabel('y+');
%     ylabel('u+');
%     legend('u+=y+','Log Law',qdir{l},'Location', 'NorthWest');
% end
% saveas(70,'loglaw_udns_filt');
% 
% 
% figure(71)
% for l=3:4
%     subplot(2,1,l-2);
%     semilogx(zplus(dz_ind,:,l),zplus(dz_ind,:,l),zplus(dz_ind,:,l),loglaw(dz_ind,:,l),zplus(dz_ind,:,l),uplus(dz_ind,:,l))
%     axis([0 max(max(zplus(dz_ind,:,:))) 0 25]);
%     grid on
%     xlabel('y+');
%     ylabel('u+');
%     legend('u+=y+','Log Law',qdir{l},'Location', 'NorthWest');
% end
% saveas(71,'loglaw_dynsmag_sigma');
% 
% l = 5;
% figure(72)
% semilogx(zplus(dz_ind,:,l),zplus(dz_ind,:,l),zplus(dz_ind,:,l),loglaw(dz_ind,:,l),zplus(dz_ind,:,l),uplus(dz_ind,:,l))
% axis([0 max(max(zplus(dz_ind,:,:))) 0 25]);
% grid on
% xlabel('y+');
% ylabel('u+');
% legend('u+=y+','Log Law',qdir{l},'Location', 'NorthWest');
% saveas(72,'loglaw_inter');

% symbols{1} = '--';
% symbols{2} = '-';
% symbols{2} = 'g-';
% symbols{3} = 'r-.';
% symbols{4} = 'b--';

% COMPUTE u' , v' , w' max RMS within boundary layer
qrms_span_avg(1:nx,1:nzp,1:3,1:ndir) = sum(qrms,2)/double(ny);
for n=1:ndir
    for i=1:nx
        ks(i,n) = find(z_scaled<=3.5*dstar(i,n), 1, 'first');
%         tmp(i,ks(i,n):nzp,1:3,n) = max(qrms(i,:,ks(i,n):nzp,1:3,n),[],2);%/double(ny);
%         qmaxrms(i,1:3,n) = max(tmp(i,ks(i,n):nzp,1:3,n),[],2);
        qmaxrms(i,1:3,n) = max(qrms_span_avg(i,ks(i,n):nzp,1:3,n),[],2);
    end
end


% plot qrms_max
figure(6)
plot(x_sp(1:20:1022),upmax_sp(1:20:1022),'ok',x_sp(351:10:650),upmax_sp(351:10:650),'ok','MarkerFaceColor','k','LineWidth',1.0);
% plot(x_sp,upmax_sp,'-k','LineWidth',2.0);
hold on
% plot(x_scaled(1:10:nx),qmaxrms(1:10:nx,1,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,qmaxrms(:,1,1),'-', x_scaled,qmaxrms(:,1,2),'-', x_scaled,qmaxrms(:,1,3),'-.', x_scaled,qmaxrms(:,1,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,qmaxrms(:,1,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),qmaxrms(1:10:nx,1,2),'bd','MarkerFaceColor','b','LineWidth',1.0);
hold off
% axis([0.5 7.5 0 0.275]);
axis([0.5 7.5 0 0.35]);
set(gca,'FontSize',14)
grid on
xlabel('x', 'FontSize',14.0);
ylabel('u\prime', 'FontSize',14.0);
% legend('DNS',qdir{:});
% saveas(6,'upmax');
% saveas(6,'upmax_filt_1pc','epsc');
saveas(6,'upmax_filt_v2','epsc');

figure(7)
plot(x_sp(1:20:1022),wpmax_sp(1:20:1022),'ok',x_sp(351:10:650),wpmax_sp(351:10:650),'ok','MarkerFaceColor','k','LineWidth',1.0);
% plot(x_sp,wpmax_sp,'-k','LineWidth',2.0);
hold on
% plot(x_scaled(1:10:nx),qmaxrms(1:10:nx,2,1),'bd','MarkerFaceColor','b','LineWidth',2.0);
% plot(x_scaled,qmaxrms(:,2,1),'-', x_scaled,qmaxrms(:,2,2),'-', x_scaled,qmaxrms(:,2,3),'-.', x_scaled,qmaxrms(:,2,4),'--','LineWidth',2.0);
for n=1:ndir
    plot(x_scaled,qmaxrms(:,2,n),symbols{n},'color',color{n},'LineWidth',2.0);
end
plot(x_scaled(1:10:nx),qmaxrms(1:10:nx,2,2),'bd','MarkerFaceColor','b','LineWidth',1.0);
hold off
% axis([0.5 7.5 0 0.275]);
axis([0.5 7.5 0 0.25]);
set(gca,'FontSize',14)
grid on
xlabel('x', 'FontSize',14.0);
ylabel('v\prime','FontSize',14.0);
% legend('DNS',qdir{:});
% saveas(7,'wpmax');
% saveas(7,'wpmax_filt_1pc','epsc');
saveas(7,'wpmax_filt_v2','epsc');

% plot figure 7 in Spalart & Strelets (2000)
load('spalart_profiles.mat');
% first plot <u>(z) from 0 to 0.18
% then plot d<u>/dz/max(d<u>/dz)
% then plot u'/u'_max
% at 3 different locations x=2, 2.5, 3.5
ix1 = find(x_scaled>=2.0, 1, 'first');
ix2 = find(x_scaled>=2.5, 1, 'first');
ix3 = find(x_scaled>=3.0, 1, 'first');
ix4 = find(x_scaled>=3.5, 1, 'first');
iz = find(z_scaled<=0.2, 1, 'first');

iy(1:ndir) = 13;
for n=1:ndir
% if (n==1)
%     iz = find(z_scaled<=0.04, 1, 'first');
% end
h = figure(76+n);
zr(1:nzp) = 0.0;
% umean(1:nzp) = q(ix1,:,1,n)./max(q(ix1,iz:nzp,1,n));
% % urms(1:nzp) = qrms(ix1,iy(n),:,1,n)./max(qrms(ix1,iy(n),iz:nzp,1,n));
% urms(1:nzp) = qrms_span_avg(ix1,:,1,n)./max(qrms_span_avg(ix1,iz:nzp,1,n));
% uy(1:nzp) = abs(dudz(ix1,:,n))./max(abs(dudz(ix1,iz:nzp,n)));
% plot(zr,z_scaled,'-k','LineWidth',2.0);
% % plot(umean,z_scaled,'-',urms,z_scaled,'--',uy,z_scaled,'-.', 'LineWidth',2.0);hold on
% plot(umean,z_scaled,'-',urms,z_scaled,'--', 'LineWidth',2.0);hold on
% 
% % plot Spalart's data I extracted from figure
% % plot(u_sp{1},yu_sp{1},'k*',urms_sp{1},yurms_sp{1},'kd',uy_sp{1},yuy_sp{1},'kv', 'LineWidth',2.0)
% plot(u_sp{1},yu_sp{1},'k*',urms_sp{1},yurms_sp{1},'kd', 'LineWidth',2.0)
% 
% 
% % if (n==1)
% %     iz = find(z_scaled<=0.2, 1, 'first');
% % end
% zr(1:nzp) = 1.2;
% umean(1:nzp) = q(ix2,:,1,n)./max(q(ix2,iz:nzp,1,n)) + 1.2;
umean(1:nzp) = q(ix2,:,1,n)./max(q(ix2,iz:nzp,1,n));
% umean(1:nzp) = q(ix2,:,1,n)./max(u_sp{2}()); % needs some love <--------------------------------
% urms(1:nzp) = qrms(ix2,iy(n),:,1,n)./max(qrms(ix2,iy(n),iz:nzp,1,n)) + 1.2;
% urms(1:nzp) = qrms_span_avg(ix2,:,1,n)./max(qrms_span_avg(ix2,iz:nzp,1,n)) + 1.2;
urms(1:nzp) = qrms_span_avg(ix2,:,1,n)./max(qrms_span_avg(ix2,iz:nzp,1,n));
% urms(1:nzp) = qrms(ix2,iy(n),:,1,n)./max(qrms(ix2,iy(n),iz:nzp,1,n));


uy(1:nzp) = abs(dudz(ix2,:,n))./max(abs(dudz(ix2,iz:nzp,n))) + 1.2;
plot(zr,z_scaled,'-k','LineWidth',2.0);

hold on

% plot(umean,z_scaled,'-',urms,z_scaled,'--',uy,z_scaled,'-.', 'LineWidth',2.0);
% plot(umean,z_scaled,'-',urms,z_scaled,'--',uy,z_scaled,'-.', 'LineWidth',2.0);
plot(umean,z_scaled,'-',urms,z_scaled,'--', 'LineWidth',2.0);

% plot(u_sp{2}+ 1.2,yu_sp{2},'k*',urms_sp{2}+ 1.2,yurms_sp{2},'kd',uy_sp{2}+ 1.2,yuy_sp{2},'kv', 'LineWidth',2.0)
% plot(u_sp{2}+ 1.2,yu_sp{2},'k*',urms_sp{2}+ 1.2,yurms_sp{2},'kd', 'LineWidth',2.0)
plot(u_sp{2},yu_sp{2},'ko',urms_sp{2},yurms_sp{2},'kd', 'MarkerFaceColor','k', 'LineWidth',1.0)


zr(1:nzp) = 1.5;
umean(1:nzp) = q(ix3,:,1,n)./max(q(ix3,iz:nzp,1,n)) + 1.5;
% urms(1:nzp) = qrms(ix3,iy(n),:,1,n)./max(qrms(ix3,iy(n),iz:nzp,1,n)) + 2.4;
urms(1:nzp) = qrms_span_avg(ix3,:,1,n)./max(qrms_span_avg(ix3,iz:nzp,1,n)) + 1.5;
uy(1:nzp) = abs(dudz(ix3,:,n))./max(abs(dudz(ix3,iz:nzp,n))) + 2.4;
plot(zr,z_scaled,'-k','LineWidth',2.0);
% plot(umean,z_scaled,'-',urms,z_scaled,'--',uy,z_scaled,'-.', 'LineWidth',2.0);
plot(umean,z_scaled,'-',urms,z_scaled,'--', 'LineWidth',2.0);

% plot(u_sp{3}+ 2.4,yu_sp{3},'k*',urms_sp{3}+ 2.4,yurms_sp{3},'kd',uy_sp{3}+ 2.4,yuy_sp{3},'kv', 'LineWidth',2.0)
plot(u_sp{3}+ 1.5,yu_sp{3},'ko',urms_sp{3}+ 1.5,yurms_sp{3},'kd', 'MarkerFaceColor','k', 'LineWidth',2.0)

% zr(1:nzp) = 1.5;
zr(1:nzp) = 2*1.5;
% zr(1:nzp) = 3*1.2;
umean(1:nzp) = q(ix4,:,1,n)./max(q(ix4,iz:nzp,1,n)) + 1.5*2;
% umean(1:nzp) = q(ix4,:,1,n)./max(q(ix4,iz:nzp,1,n)) + 1.5;
% urms(1:nzp) = qrms(ix4,iy(n),:,1,n)./max(qrms(ix4,iy(n),iz:nzp,1,n)) + 1.2*3;
% urms(1:nzp) = qrms_span_avg(ix4,:,1,n)./max(qrms_span_avg(ix4,iz:nzp,1,n)) + 3*1.2;
urms(1:nzp) = qrms_span_avg(ix4,:,1,n)./max(qrms_span_avg(ix4,iz:nzp,1,n)) + 2*1.5;
% urms(1:nzp) = qrms(ix4,iy(n),:,1,n)./max(qrms(ix4,iy(n),iz:nzp,1,n)) + 1.5;


uy(1:nzp) = abs(dudz(ix4,:,n))./max(abs(dudz(ix4,iz:nzp,n))) + 1.2*3;
plot(zr,z_scaled,'-k','LineWidth',2.0);
% plot(umean,z_scaled,'-',urms,z_scaled,'--',uy,z_scaled,'-.', 'LineWidth',2.0);
plot(umean,z_scaled,'-',urms,z_scaled,'--', 'LineWidth',2.0);

% plot(q(ix2,:,1,n)+1.2,z_scaled,'-',dudz(ix2,:,n)./max(dudz(ix2,:,n))+1.2,z_scaled,'-.',qrms(ix2,1,:,1,n)./max(qrms(ix2,1,:,1,n))+1.2,z_scaled,'--');
% plot(q(ix3,:,1,n)+2.4,z_scaled,'-',dudz(ix3,:,n)./max(dudz(ix3,:,n))+2.4,z_scaled,'-.',qrms(ix3,1,:,1,n)./max(qrms(ix3,1,:,1,n))+1.2,z_scaled,'--');

% plot(u_sp{4}+ 1.2*3,yu_sp{4},'k*',urms_sp{4}+ 1.2*3,yurms_sp{4},'kd',uy_sp{4}+ 1.2*3,yuy_sp{4},'kv', 'LineWidth',2.0)
% plot(u_sp{4}+ 1.2*3,yu_sp{4},'k*',urms_sp{4}+ 1.2*3,yurms_sp{4},'kd', 'LineWidth',2.0)
plot(u_sp{4}+ 2*1.5,yu_sp{4},'ko',urms_sp{4}+ 2*1.5,yurms_sp{4},'kd', 'MarkerFaceColor','k', 'LineWidth',1.0)


hold off
axis([-0.25 3*1.5-0.25 0 0.2]);
set(gca,'FontSize',14)
daspect([20 1 1]);
grid on
ylabel('y', 'FontSize',14.0);
% title(strcat('Profiles for LES with: ',qdir{n}));
% legend('','U','u\prime/u\prime_{max}','|U_y|/U_{ymax}');
str = strcat('profiles_1pc_',qdir{n});
saveas(h,str,'epsc');
end

% umeanf = fit(z_scaled,umean','smoothingspline');
% urmsf = fit(urms',z_scaled,'smoothingspline');
% uyf = fit(uy',z_scaled,'smoothingspline');
% figure(760)
% plot(umeanf',umean,z_scaled,'-');
% legend('U_{smooth}','U');
% % legend('zero','U','U_{smooth}');
% figure(761)
% plot(zr,z_scaled,'-k',urms,z_scaled,'-',urmsf,z_scaled,'--');
% legend('zero','urms','urms_{smooth}');
% figure(762)
% plot(zr,z_scaled,'-k',uy,z_scaled,'-',uyf,z_scaled,'--');
% legend('zero','Uy','Uy_{smooth}');
% figure(8)
% plot(x_sp,wpmax_sp,'-k');
% hold on
% for n=1:ndir
%     plot(x_scaled,qmaxrms(:,2,n),symbols{n});
% end
% xlabel('x');
% ylabel('w^{`}');
% legend('DNS',qdir{:});

% 
% % plot p(x,z->0)
% figure(10)
% plot(x_scaled,p(:,nzp),x_scaled,p(:,nz),x_scaled,p(:,nz-1),x_scaled,p(:,1))
% legend(num2str(zpts(nzp)),num2str(zpts(nz)),num2str(zpts(nz-1)),num2str(zpts(1)));
% ylabel('p(x)');
% xlabel('x');
% title('p(x) at diff z loc');
% saveas(10,'p(x)_vs_z');
% 
% % plot vertical profiles of p(z)
% figure(11)
% plot(p(nx/6:nx/10:nx,:),z_scaled);
% % plot(p(1:50:nx,nz-10:nzp),z_scaled(nz-10:nzp));
% % plot(p(1:50:nx,1:nz-10),z_scaled(1:nz-10),'x');
% xlabel('p(z)');
% ylabel('y');
% title('p(z) at diff x loc');
% saveas(11,'p(z)_vs_x');
% 
% % u(z)
% figure(5)
% plot(u(1:50:nx,1:nzp),z_scaled(1:nzp));
% xlabel('u(z) [cm/s]');
% ylabel('y');
% title('u(z) at diff x loc');
% saveas(5,'u(z)_vs_x');
% 
% % u(x)
% figure(6)
% plot(x_scaled,u(:,1:4:nz));
% ylabel('u(x) [cm/s]');
% xlabel('x');
% title('u(x) at diff z loc');
% saveas(6,'u(x)_vs_z');
% 
% % w(x)
% figure(7)
% plot(x_scaled,w(:,1:4:nz));
% ylabel('w(x) [cm/s]');
% xlabel('x');
% title('w(x) at diff z loc');
% saveas(7,'w(x)_vs_z');
% 
% % w(z)
% figure(8)
% plot(w(1:50:nx,1:nzp),z_scaled(1:nzp));
% xlabel('w(z) [cm/s]');
% ylabel('y');
% title('w(z) at diff x loc');
% saveas(8,'w(z)_vs_x');
% 
% 
% % spanwise plots
% wy(:,:) = win(:,:,nzp-20);
% uy(:,:) = uin(:,:,nzp-20);
% vy(:,:) = vin(:,:,nzp-20);
% 
% 
% figure(30)
% % contour(xy,yx,wy);
% plot(y_scaled,wy(nx/3:nx/10:nx,:))
% % colorbar;
% xlabel('y/H');
% ylabel('w(y)');
% title('w(y) at different x locations');
% % ylabel('y/H');
% % title('time-avg contour plot w(x,y,nzp-25)');
% saveas(30,'w(y)_vs_x');
% 
% figure(31)
% % contour(xy,yx,wy);
% plot(y_scaled,vy(nx/3:nx/10:nx,:))
% % colorbar;
% xlabel('y/H');
% ylabel('v(y)');
% title('v(y) at different x locations');
% % ylabel('y/H');
% % title('time-avg contour plot w(x,y,nzp-25)');
% saveas(31,'v(y)_vs_x');
% 
% 
% figure(32)
% % contour(xy,yx,wy);
% plot(y_scaled,uy(nx/3:nx/10:nx,:))
% % colorbar;
% xlabel('y/H');
% ylabel('u(y)');
% title('u(y) at different x locations');
% % ylabel('y/H');
% % title('time-avg contour plot w(x,y,nzp-25)');
% saveas(32,'u(y)_vs_x');
% 
% 
% boundary conditions
% figure(101)
% % plot(x_scaled(1:10:nx),q(1:10:nx,1,3,4),'ok',x_scaled(50:90),q(50:90,1,3,4),'ok','MarkerFaceColor','k','LineWidth',2.0);
% plot(x_scaled(1:20:nx),q(1:20:nx,1,3,4),'ok',x_scaled(51:5:90),q(51:5:90,1,3,4),'ok','MarkerFaceColor','k','LineWidth',2.0);
% hold on
% plot(x_scaled,q(:,1,3,4),'-k','LineWidth',2.0);
% plot(x_scaled(1:20:nx),q(1:20:nx,nzp,3,4),'vk',x_scaled(nx-49:5:nx),q(nx-49:5:nx,nzp,3,4),'vk','LineWidth',2.0);
% plot(x_scaled,q(:,nzp,3,4),'-k','LineWidth',2.0);
% % plot(x_scaled(1:10:nx),q(1:10:nx,nzp,3,4),'vk',x_scaled(nx-50:5:nx),q(nx-50:5:nx,nzp,3,4),'vk','LineWidth',2.0);
% 
% plot(x_scaled(1:20:nx), q(1:20:nx,1,1,1)-1.0,'bd','MarkerFaceColor','b', 'LineWidth',2.0);
% plot(x_scaled, q(:,1,1,1)-1.0,'-',x_scaled, q(:,1,1,2)-1.0,'-',x_scaled, q(:,1,1,3)-1.0,'-.',x_scaled, q(:,1,1,4)-1.0,'--','LineWidth',2.0);
% hold off
% % plot(x_scaled,q(:,nzp,3,4),'-.k',x_scaled,q(:,1,1,4)-1.,'--k',x_scaled,q(:,1,3,4),'k','LineWidth',2.0);
% axis([0.5 10 -0.6 0.8]);
% set(gca,'FontSize',14)
% grid on
% xlabel('x','FontSize',14);
% ylabel('velocity','FontSize',14);
% saveas(101,'bc_filt_1pc','epsc');
% legend('u(Y) - u_{\infty}','v(Y)','v(0)');